import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => MyProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Snackbar+Provider',
        home: MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  final providerVariable = MyProvider();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Snackbar+Provider'),
      ),
      body: Builder(
        builder: (context) => Center(
          child: RaisedButton(
            onPressed: () {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(providerVariable.text),
                ),
              );
            },
            child: Text('Show snackbar'),
          ),
        ),
      ),
    );
  }
}

class MyProvider with ChangeNotifier {
  String _text = "Waaaaaaaaaagh";

  String get text => _text;
}
